https://github.com/adrianhajdin/project_shareme_social_media
Styles & Other Code: https://gist.github.com/adrianhajdin/a53fdfe71225af2f550b853ae983f0d0
Assets: https://drive.google.com/drive/folders/1BO06XeOPLDWYdPyFYP_Rhq4AokgFqY7q
Google Console - https://www.console.cloud.google.com 
Tailwind: https://tailwindcss.com/docs/guides/create-react-app
Tutorial: https://www.youtube.com/watch?v=1RHDhtbqo94
Time Stamps
00:00 Intro
00:04:43 Project Setup
00:24:28 Frontend Setup
00:39:58 Auth - Login/Register
01:08:26 Home, Sidebar & Navbar
01:59:16 Feed View
02:24:30 Pin Component
02:58:34 Create Pin
03:30:18 Pin Details
04:02:13 User Profile
04:20:17 Search & Categories
04:30:52 Deployment

