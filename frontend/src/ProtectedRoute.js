import React from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import { useAuth } from "./context/auth.context";
import SweetAlert from "react-bootstrap-sweetalert";
import Home from "./container/Home";
import About from "./container/About";
import Logout from "./components/Logout";

export const ProtectedRoute = (/* { ...rest } */) => {
   const navigate = useNavigate();
   let { user } = useAuth();
   user = JSON.parse(user);
   console.log("user from protected route", user);

   if (!user || !user.token || user.token === "") {
      return (
         <SweetAlert
            title="You must be signed in!"
            onCancel={() => navigate("/login")}
            onConfirm={() => navigate("/login")}
            confirmBtnCssClass={"px-5"}
         />
      );
   }

   return (
      <Routes>
         <Route path="/home" element={<Home />} />
         <Route path="/about" element={<About />} />
         <Route path="/logout" element={<Logout />} />
      </Routes>
   );
};
