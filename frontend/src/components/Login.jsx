import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AuthApi from "../api/auth";
import { useAuth } from "../context/auth.context";
import shareVideo from "../assets/share.mp4";
import logo from "../assets/logowhite.png";
import LoginForm from "./LoginForm";
const Login = () => {
   const [username, setUsername] = useState("");
   const [password, setPassword] = useState("");
   const { user, setUser } = useAuth();
   const navigate = useNavigate();

   //let { user } = useAuth();
   //user = JSON.parse(user);

   useEffect(() => {
      //Runs only on the first render
      //let user = localStorage.getItem("user");
      //console.log("user token from login", user.token);
      let didCancel = user == null;
      const goToHome = () => navigate("/home");
      if (!didCancel) {
         goToHome();
      }
      return () => {
         didCancel = true;
      };
   }, [navigate]);

   const login = async (event) => {
      if (event) {
         event.preventDefault();
      }

      //handle exceptions like: no email entered, no password entered, here.
      await AuthApi.Login({
         username,
         password,
      })
         .then((response) => setProfile(response))
         .catch((err) => console.log("ERROR", err));
      /* try {
         let response = await AuthApi.Login({
            username,
            password,
         });
         if (response.data && response.data.success === false) {
            //display error coming from server
            return setError(response.data.msg);
         }
         return setProfile(response);
      } catch (err) {
         //display error originating from server / other sources
         console.log(err);
         if (err.response) {
            return setError(err.response.data.msg);
         }
         return setError("There has been an error.");
      } */
   };
   const setProfile = (response) => {
      let user = { ...response.data.user };
      user.token = response.data.token;
      user = JSON.stringify(user);
      //setUser is imported from the useAuth React Context
      setUser(user);
      //also set the user in local storage
      localStorage.setItem("user", user);
      return navigate("/home");
   };
   const setError = (error) => {
      console.log(error);
   };

   return (
      <div className="flex justify-start items-center flex-col h-screen">
         <div className="relative w-full h-full">
            <video
               src={shareVideo}
               type="video/mp4"
               loop
               controls={false}
               muted
               autoPlay
               className="w-full h-full object-cover"
            />
            <div className="flex flex-col justify-center items-center top-0 right-0 left-0 bottom-0 bg-blackOverlay">
               <div className="p-5">
                  <img src={logo} width="130px" alt="logo" />
               </div>
            </div>
         </div>
         <LoginForm
            setUsername={setUsername}
            setPassword={setPassword}
            handleLogin={login}
         />
      </div>
   );
};

export default Login;
