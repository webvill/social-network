import { useNavigate } from "react-router-dom";
const Logout = () => {
   localStorage.clear();
   return useNavigate("/login");
};

export default Logout;
