import React, { useState, useRef, useEffect } from "react";
import { Link, Route, Routes } from "react-router-dom";
import { HiMenu } from "react-icons/hi";
import { AiFillCloseCirlce } from "react-icons/ai";
import { Sidebar, UserProfile } from "../components";
import { useAuth } from "../context/auth.context";
import logo from "../assets/logo.png";
import Pins from "./Pins";

const Home = () => {
   const [toggleSidebar, setToggleSidebar] = useState(false);
   /* const userInfo =
      localStorage.getItem("user") !== "undefined"
         ? JSON.parse(localStorage.getItem("user "))
         : localStorage.clear(); */

   let { user } = useAuth();
   user = JSON.parse(user);
   console.log("user from home", user);
   return (
      <>
         <div className="flex bg-gray-50 med:flex-row flex-col h-screen transition-height duration-75 ease-out">
            <Sidebar />
            <div className="hidden md:flex h-screen flex-initial">
               <HiMenu
                  fontSize={40}
                  className="cursor-pointer"
                  onClick={() => setToggleSidebar(false)}
               />
               <Link to="/">
                  <img src={logo} alt="logo" className="w-28" />
               </Link>
               <Link to={`user-profile/${user?.id}`}>
                  <img src={logo} alt="logo" className="w-28" />
               </Link>
               {toggleSidebar && (
                  <div className="fixed w-4/5 bg-white h-screen overflow-y-auto shadow-md z-10 animate-slide-in">
                     <div className="absolute w-full flex justify-end  items-center p-2">
                        <AiFillCloseCirlce fontSize={30} className="cursor-pointer" onClick={() => setToggleSidebar()/>
                     </div>
                     </div>
               )}
            </div>
         </div>
         <Link to="/about">About</Link>
         <Link to="/logout">Logout</Link>
      </>
   );
};

export default Home;
