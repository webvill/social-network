import React from "react";
import { AuthProvider } from "./context/auth.context";
import { Routes, Route } from "react-router-dom";
import { ProtectedRoute } from "./ProtectedRoute";
import Login from "./components/Login";

let user = localStorage.getItem("user");
user = JSON.parse(user);
console.log("user from app", user);
const App = () => (
   <AuthProvider userData={user}>
      <Routes>
         <Route path="/login" element={<Login />} />
         <Route path="/*" element={<ProtectedRoute />} />
      </Routes>
   </AuthProvider>
);

export default App;
