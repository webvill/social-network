using Microsoft.AspNetCore.Mvc;
using Pomelo.EntityFrameworkCore;
using Dtos;
using Models;
using Repositories;
using Interfaces;

namespace Controllers;

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repository;
        public UsersController(IUserRepository repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<IEnumerable</* UserDto */AppUser>> GetUsers()
        {
            return _repository.GetAll();
            //return new List<UserDto>(){new UserDto{Username = "Hans", Token = "string"}};
        }
    }