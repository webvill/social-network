using Models;

namespace Interfaces;
public interface IUserRepository : IGenericRepository<AppUser>
{
    
}