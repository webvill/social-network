using Interfaces;
using Models;

namespace Repositories;

public class UserRepository : GenericRepository<AppUser>, IUserRepository
{
        public UserRepository(ApplicationDbContext context):base(context)
    {
    }
}